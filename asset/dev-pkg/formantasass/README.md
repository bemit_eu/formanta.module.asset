# FormantaSass: Core

A Sass Library with style output or not, complete themes could be build using only config values.

It is split into two parts:

- [FormantaSass](https://bitbucket.org/bemit_eu/formantasass), this repo, containing full sets of reusable components
- [FormantaSass: Core](https://bitbucket.org/bemit_eu/formantasass-core), core logic, basic style and utility functions
- Core is independent, whereas FormantaSass (this) relies on Core

## Install

With composer:
```
composer require bemit/formantasass
composer require bemit/formantasass-core
```

Manual installation:

Download or clone both repo's `master` into one folder, it is needed that both projects are in the same folder with their names.

```text
/vendor/bemit/formantasass
/vendor/bemit/formantasass-core
``` 
or
```text
/asset/formantasass
/asset/formantasass-core
``` 

Clone:

```bash
git clone https://bitbucket.org/bemit_eu/formantasass.git ./vendor/bemit/formantasass
git clone https://bitbucket.org/bemit_eu/formantasass-core.git ./vendor/bemit/formantasass-core
```

Download:

- FormantaSass
    - [master zip](https://bitbucket.org/bemit_eu/formantasass/get/master.zip)
    - [develop zip](https://bitbucket.org/bemit_eu/formantasass/get/develop.zip)
- FormantaSass-Core
    - [master zip](https://bitbucket.org/bemit_eu/formantasass-core/get/master.zip)
    - [develop zip](https://bitbucket.org/bemit_eu/formantasass-core/get/develop.zip)
    
## Ready To Use

For example, integrated in [`Flood\Canal`](https://painttheweb.de/flood-canal/setup-and-run-a-project)

### Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

### Information, Support and Documentation

More informations, contacts and support for FormantaSass could be found under <https://formanta.bemit.eu> the documentation is located at <https://help.formanta.bemit.eu>.

# Made by

Michael Becker, mb@project.bemit.eu

The Logo was made by friends of Born & Partner <http://werbeagentur-born.de>

## Copyright

    2015 - 2018 | Michael Becker, bemit UG (haftungsbeschränkt) - project@we.bemit.eu